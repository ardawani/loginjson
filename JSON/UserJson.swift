//
//  UserJson.swift
//  JSON
//
//  Created by Ardavan Izadiyar on 21/9/16.
//  Copyright © 2016 Ardavan Izadiyar. All rights reserved.
//

import Foundation

class UserJson {

    var ID: Int = 0
    var Name: String = ""
    var LastName: String = ""
    var IC: String = ""
    var Photo: String = ""
    var Email: String = ""
    var Activation: Bool = false
    var Status: String = ""
    
}

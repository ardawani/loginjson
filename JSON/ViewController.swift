//
//  ViewController.swift
//  JSON
//
//  Created by Ardavan Izadiyar on 21/9/16.
//  Copyright © 2016 Ardavan Izadiyar. All rights reserved.
//

import UIKit

// MARK: - Global vars
var thisUser = [UserJson]()


class myViewController: UIViewController {
    
    // MARK: - Model
    let JSON = JSONModel()
    
    @IBOutlet var alert: UILabel!
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    
    @IBAction func login(_ sender: UIButton) {
        
        if JSON.PostJson(user: username.text!, pass: password.text!) {
            
            // I want these lines run after parsing JSON 
            alert.text = "Yes"
            performSegue(withIdentifier: "welcome", sender: nil)
            
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        username.text = "a@a.com" //"6140612e636f6d"
        password.text = "aaaaaa"
        
        alert.text = ""
        
    }
    
}

